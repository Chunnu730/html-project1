async function locationData() {
  const response = await fetch("./data.js");
  const resJson = await response.json();
  resJson.forEach((element) => {
    if (element.location === "Indonesia") {
      const newElement = document.createElement("tr");
      newElement.innerHTML = `<td>${element.id}</td>
        <td>${element.first_name}</td>
        <td>${element.last_name}</td>
        <td>${element.cost}</td>
        <td>${element.location}</td>
        <td>${element.depreciation_value}</td>
        <td>${element.owner_number}</td>
        <td>${element.car}</td>`;
      document.querySelector("#table-content").appendChild(newElement);
    }
  });
}

locationData();
