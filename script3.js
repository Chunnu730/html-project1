async function cost() {
  const response = await fetch("http://127.0.0.1:5500/data.js");
  const resJson = await response.json();
  const t_cost = await resJson.reduce((accu, current) => {
    if (!accu[current.location]) {
      accu[current.location] = Number(current.cost.slice(1));
    } else {
      accu[current.location] += Number(current.cost.slice(1));
    }
    return accu;
  }, []);
  Object.entries(t_cost).forEach((element) => {
    const newElement = document.createElement("tr");
    newElement.innerHTML = `<td>${element[0]}</td>
        <td>${element[1]}</td>`;
    document.querySelector("#table-content").appendChild(newElement);
  });
}

cost();
