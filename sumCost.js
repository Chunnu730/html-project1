const data = require('./data')
function sumCost(accu, current){
   accu += Number(current['cost'].slice(1))
   return accu;
}
const sum = data.reduce(sumCost, 0)
console.log(sum);