async function insertData() {
  const response = await fetch("http://127.0.0.1:5500/data.js");
  const resJson = await response.json();
  resJson.forEach((element) => {
    const newElement = document.createElement("tr");
    newElement.innerHTML = `<td>${element.id}</td>
        <td>${element.first_name}</td>
        <td>${element.last_name}</td>
        <td>${element.cost}</td>
        <td>${element.location}</td>
        <td>${element.depreciation_value}</td>
        <td>${element.owner_number}</td>
        <td>${element.car}</td>`;
    document.querySelector("#table-content").appendChild(newElement);
  });
}
insertData();
