[{ "id": 1, "first_name": "Pierette", "last_name": "Lincke", "cost": "$8.06", "location": "China", "depreciation_value": 8.76, "owner_number": 1, "car": "Viper" },
{ "id": 2, "first_name": "Brendin", "last_name": "Shelf", "cost": "$3.44", "location": "Indonesia", "depreciation_value": 16.23, "owner_number": 1, "car": "Thunderbird" },
{ "id": 3, "first_name": "Robinett", "last_name": "Bradbeer", "cost": "$8.78", "location": "Russia", "depreciation_value": 21.39, "owner_number": 9, "car": "SC" },
{ "id": 4, "first_name": "Alwyn", "last_name": "Ditter", "cost": "$6.71", "location": "Peru", "depreciation_value": 55.66, "owner_number": 4, "car": "Excel" },
{ "id": 5, "first_name": "Venus", "last_name": "Pailin", "cost": "$0.79", "location": "United States", "depreciation_value": 64.38, "owner_number": 6, "car": "Expedition" },
{ "id": 6, "first_name": "Mack", "last_name": "Garvin", "cost": "$8.58", "location": "Yemen", "depreciation_value": 80.8, "owner_number": 7, "car": "E-Series" },
{ "id": 7, "first_name": "Peg", "last_name": "Lammerich", "cost": "$6.43", "location": "China", "depreciation_value": 58.56, "owner_number": 10, "car": "Coupe GT" },
{ "id": 8, "first_name": "Gilda", "last_name": "Kulis", "cost": "$9.25", "location": "China", "depreciation_value": 70.45, "owner_number": 2, "car": "S-Class" },
{ "id": 9, "first_name": "Maurine", "last_name": "Grzegorzewicz", "cost": "$6.49", "location": "China", "depreciation_value": 11.04, "owner_number": 2, "car": "Mustang" },
{ "id": 10, "first_name": "Marnia", "last_name": "Kenwrick", "cost": "$3.14", "location": "Nigeria", "depreciation_value": 88.64, "owner_number": 3, "car": "C70" },
{ "id": 11, "first_name": "Dotti", "last_name": "Prettjohn", "cost": "$5.73", "location": "Senegal", "depreciation_value": 71.08, "owner_number": 8, "car": "Familia" },
{ "id": 12, "first_name": "Stormie", "last_name": "Whyke", "cost": "$4.95", "location": "Brazil", "depreciation_value": 2.17, "owner_number": 8, "car": "Caliber" },
{ "id": 13, "first_name": "Kristi", "last_name": "Herety", "cost": "$9.46", "location": "Argentina", "depreciation_value": 37.72, "owner_number": 9, "car": "RL" },
{ "id": 14, "first_name": "Gale", "last_name": "Ridgewell", "cost": "$5.61", "location": "United States", "depreciation_value": 39.1, "owner_number": 4, "car": "Grand Vitara" },
{ "id": 15, "first_name": "Geno", "last_name": "Korda", "cost": "$0.30", "location": "China", "depreciation_value": 11.61, "owner_number": 7, "car": "MX-5" },
{ "id": 16, "first_name": "Lynda", "last_name": "McOmish", "cost": "$4.79", "location": "France", "depreciation_value": 84.93, "owner_number": 3, "car": "Aerostar" },
{ "id": 17, "first_name": "Sarah", "last_name": "Handlin", "cost": "$7.06", "location": "Russia", "depreciation_value": 56.3, "owner_number": 9, "car": "A4" },
{ "id": 18, "first_name": "Ralph", "last_name": "Gainsbury", "cost": "$9.97", "location": "Bosnia and Herzegovina", "depreciation_value": 56.07, "owner_number": 6, "car": "Galant" },
{ "id": 19, "first_name": "Ricoriki", "last_name": "Calderon", "cost": "$0.35", "location": "Brazil", "depreciation_value": 28.54, "owner_number": 8, "car": "Tahoe" },
{ "id": 20, "first_name": "Candida", "last_name": "Proger", "cost": "$1.72", "location": "Thailand", "depreciation_value": 59.61, "owner_number": 2, "car": "XC60" },
{ "id": 21, "first_name": "Daron", "last_name": "Bodell", "cost": "$0.50", "location": "Colombia", "depreciation_value": 75.03, "owner_number": 3, "car": "M-Class" },
{ "id": 22, "first_name": "Erie", "last_name": "Meak", "cost": "$5.08", "location": "United States", "depreciation_value": 45.41, "owner_number": 10, "car": "Alero" },
{ "id": 23, "first_name": "Dom", "last_name": "Phinnis", "cost": "$4.88", "location": "Russia", "depreciation_value": 89.92, "owner_number": 2, "car": "Altima" },
{ "id": 24, "first_name": "Charline", "last_name": "Madoc-Jones", "cost": "$7.29", "location": "Azerbaijan", "depreciation_value": 26.8, "owner_number": 4, "car": "SLS-Class" },
{ "id": 25, "first_name": "Katee", "last_name": "Malsher", "cost": "$1.24", "location": "Indonesia", "depreciation_value": 63.78, "owner_number": 6, "car": "Trooper" },
{ "id": 26, "first_name": "Linzy", "last_name": "Forrestall", "cost": "$7.80", "location": "Croatia", "depreciation_value": 53.61, "owner_number": 7, "car": "Mighty Max" },
{ "id": 27, "first_name": "Mohammed", "last_name": "Enticott", "cost": "$8.90", "location": "Macedonia", "depreciation_value": 14.8, "owner_number": 2, "car": "Fiesta" },
{ "id": 28, "first_name": "Timoteo", "last_name": "Hovel", "cost": "$9.37", "location": "United States", "depreciation_value": 13.7, "owner_number": 7, "car": "Cooper Clubman" },
{ "id": 29, "first_name": "Chickie", "last_name": "Tugwell", "cost": "$4.31", "location": "Czech Republic", "depreciation_value": 69.28, "owner_number": 2, "car": "Aerostar" },
{ "id": 30, "first_name": "Dorelia", "last_name": "Jarrette", "cost": "$5.20", "location": "Poland", "depreciation_value": 28.6, "owner_number": 3, "car": "Mirage" }]